# This file is part of the SV-Benchmarks collection of verification tasks:
# https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
#
# SPDX-FileCopyrightText: 2007-2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

The benchmarks in this directory were submitted by Haeun Lee <haeun.lee@kaist.ac.kr> and Hee Dong Yang <heedong@softsec.kaist.ac.kr>.

Programs were synthesized based on randomly generated mazes using Fuzzle (https://github.com/SoftSec-KAIST/Fuzzle).

Fuzzle and Fuzzle-generated programs are described in: "Fuzzle: Making a Puzzle for Fuzzers" (ASE 2022).
