# This file is part of the SV-Benchmarks collection of verification tasks:
# https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
#
# SPDX-FileCopyrightText: 2024 Lei Bu
#
# SPDX-License-Identifier: Apache-2.0

These benchmarks came from the paper

Lei Bu, Yongjuan Liang, et al.:
Machine Learning Steered Symbolic Execution Framework for Complex Software Code. Formal Aspects of Computing, 2021

In the paper, the authors evaluate their technique by programs with complex path conditions, like nonlinear constraints. These benchmarks were collected from 4 sets of real case benchmarks, e.g., coral. We have modified them a little bit to test C verifiers.

